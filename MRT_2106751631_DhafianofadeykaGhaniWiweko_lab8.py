# Dhafiano Fadeyka Ghani Wiweko
# 2106751631
# DDP 1
# LAB 8
class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        self.__tipe = tipe

    kuesponge = type("",
                      (),
                      {"counter": 0,
                       "__init__": kue1_init,
                       "sayHello": lambda self: "Hi, I am " + self.__tipe}) \
    kuekeju = type("",
                             (),
                             {"counter": 0,
                              "__init__": kue2_init,
                              "sayHello": lambda self: "Hi, I am " + self.__tipe})
    kuebuah = type("",
                     (),
                     {"counter": 0,
                      "__init__": kue3_init,
                      "sayHello": lambda self: "Hi, I am " + self.__tipe})

        # TODO: Implementasikan getter untuk tipe!
        pass

    def get_harga(self):
        if kuesponge <= self.harga < 2500:
            return self.harga
        elif kuekeju <= self.amount < 3000:
            return self.harga
        elif kuebuah <= self.harga < 3500:
            return self.harga
        # TODO: Implementasikan getter untuk harga!
        pass
    
    def get_tulisan(self):
        a = kuesponge()
        b = kuekeju()
        c = kuebuah()
        d = 2500
        e = 3000
        f = 3500
        # TODO: Implementasikan getter untuk tulisan!
        pass
    
    def get_angka_lilin(self):
        import random
        get_angka_lilin = []

        for i in range(0, 4):

        i = random.randint(1, 30)

        get_angka_lilin.append(q)
        # TODO: Implementasikan getter untuk angka_lilin!
        pass
    
    def get_topping(self):
        self.__topping = topping
     strawberry = type("",
                         (),
                         {"counter": 0,
                          "__init__": top1_init,
                          "sayHello": lambda self: "Hi, I am " + self.__topping}) \
    cherry_blueic= type("",
                               (),
                               {"counter": 0,
                                "__init__": top2_init,
                                "sayHello": lambda self: "Hi, I am " + self.__topping})
    apple_grape = type("",
                       (),
                       {"counter": 0,
                        "__init__": top3_init,
                        "sayHello": lambda self: "Hi, I am " + self.__topping})
        # TODO: Implementasikan getter untuk topping!
        pass

class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        self.tulisan = tulisan
        self.angka_lilin = angka_lilin
        self.toping = toping
        self.rasa = rasa
        self.warna_frosting = warna_frosting
        self.harga = 2500
        # TODO: Implementasikan constructor untuk class ini!
        pass
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    
class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        self.tulisan = tulisan
        self.angka_lilin = angka_lilin
        self.topping = topping
        self.jenis_kue_keju = jenis_kue_keju
        self.harga = 3000
        # TODO: Implementasikan constructor untuk class ini!
        pass

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!

class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        self.tulisan = tulisan
        self.angka_lilin = angka_lilin
        self.topping = topping
        self.jenis_kue_keju = jenis_kue_buah
        self.harga = 3500
        # TODO: Implementasikan constructor untuk class ini!
        pass
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!

# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!

    pass

# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    pass

# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    pass

# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()